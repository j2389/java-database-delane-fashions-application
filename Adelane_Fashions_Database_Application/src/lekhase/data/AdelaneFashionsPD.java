package lekhase.data;

import java.util.ArrayList;

/**
 *
 * @author JM Lekhase
 */
public class AdelaneFashionsPD {
//Declaring Variables

    private String storeNo;
    private String city;
    private double sales;

    public enum Province {
        GP, FS, NC, L, WC, NW, KZN, MP, EC
    };//Enumarator for Province

    public enum ownership {
        C, F
    };//Enumarator for Ownership

    private Province province;
    private ownership ownership;

    //Default Constructor
    public AdelaneFashionsPD() {
        storeNo = "";
        city = "";
        sales = 0.0;
        province = null;
        ownership = null;
    }

    //Overload Constructor
    public AdelaneFashionsPD(String storeNo, String city, double sales, Province province, ownership ownership) {
        setCity(city);
        setOwnership(ownership);
        setStoreNo(storeNo);
        setProvince(province);
        setSales(sales);
    }

    //Getters
    public String getStoreNo() {
        return storeNo;
    }

    public String getCity() {
        return city;
    }

    public double getSales() {
        return sales;
    }

    public Province getProvince() {
        return province;
    }

    public ownership getOwnership() {
        return ownership;
    }

    //Setters
    public void setStoreNo(String storeNo) {
        this.storeNo = storeNo;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setSales(double sales) {
        //validation
        if (sales < 0) {
            throw new IllegalArgumentException("sales cannot be a negative value");
        } else {
            this.sales = sales;
        }
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public void setOwnership(ownership ownership) {
        this.ownership = ownership;
    }

    //Display Client Information
    @Override
    public String toString() {
        return storeNo + "\t\t" + city + "\t\t\t" + province + "\t\t\t" + ownership + "\t\t\t" + "R " + sales + "\n";
    }

    //Database Methods
    public void initConnection() throws DataStorageException {
        AdelaneFashionsDA.initConnection();
    }

    public void terminate() throws DataStorageException {
        AdelaneFashionsDA.terminate();
    }

    public ArrayList<AdelaneFashionsPD> returnAllData() throws NotFoundException {
        return AdelaneFashionsDA.returnAllData();
    }

    public ArrayList<AdelaneFashionsPD> ReturnProvince(String Province) throws NotFoundException {
        return AdelaneFashionsDA.ReturnProvince(Province);
    }

    public double CalculateTotalSales() throws NotFoundException {
        return AdelaneFashionsDA.CalculateTotalSales();

    }

    public double CalculateTotalCompanyOwnedSales() throws NotFoundException {
        return AdelaneFashionsDA.CalculateTotalCompanyOwnedSales();
    }

    public double CalculateTotalFranchiseSales() throws NotFoundException {
        return AdelaneFashionsDA.CalculateTotalFranchiseSales();
    }

    public void RemoveStore(String StoreNo) throws NotFoundException {
        AdelaneFashionsDA.RemoveStore(StoreNo);
    }

    public void IncreasePrices(double percentage) throws NotFoundException {
        AdelaneFashionsDA.IncreasePrices(percentage);
    }
    

    public ArrayList<String> ReturnStoreNo() throws NotFoundException {
        return AdelaneFashionsDA.ReturnStoreNo();

    }

    public void addNewStore(AdelaneFashionsPD objPD) throws NotFoundException {
        AdelaneFashionsDA.addNewStore(objPD);
    }
}
