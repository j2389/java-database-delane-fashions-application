package lekhase.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author JM Lekhase
 */
public class AdelaneFashionsDA {

    private static Connection con;
    private static PreparedStatement ps;
    private static ResultSet rs;
    private static final ArrayList<String> arStoreNo = new ArrayList<>();
    private static final ArrayList<AdelaneFashionsPD> arProduct = new ArrayList<>();

    public static void initConnection() throws DataStorageException {
        final String username = "root";
        final String password = "";
        final String url = "jdbc:mysql://localhost:3306/AdelaneFashionDB";
        final String driver = "com.mysql.cj.jdbc.Driver";
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            throw new DataStorageException("Database driver is missing\n");
        } catch (SQLException e) {
            throw new DataStorageException("Connection failed" + e.getMessage()
            );
        }
    }//Connecting Database

    public static void terminate() throws DataStorageException {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            throw new DataStorageException("Termination Successful " + e.getMessage());
        }
    }//Termination

    public static ArrayList<AdelaneFashionsPD> returnAllData() throws NotFoundException {
        arProduct.clear();
        try {
            ps = con.prepareStatement("select * From tblAdelaneFashions");
            rs = ps.executeQuery();
            while (rs.next()) {
                arProduct.add(new AdelaneFashionsPD(rs.getString("StoreNumber"),
                        rs.getString("City"),
                        rs.getDouble("Sales"),
                        AdelaneFashionsPD.Province.valueOf(rs.getString("Province")),
                        AdelaneFashionsPD.ownership.valueOf(rs.getString("Ownership")
                        )));
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }

        return arProduct;
    }//Returning all data stored

    public static ArrayList<AdelaneFashionsPD> ReturnProvince(String Province) throws NotFoundException {
        arProduct.clear();
        try {
            ps = con.prepareStatement("select * From tblAdelaneFashions Where Province = ? ");
            ps.setString(1, Province);
            rs = ps.executeQuery();
            while (rs.next()) {
                arProduct.add(new AdelaneFashionsPD(rs.getString("StoreNumber"),
                        rs.getString("City"),
                        rs.getDouble("Sales"),
                        AdelaneFashionsPD.Province.valueOf(rs.getString("Province")),
                        AdelaneFashionsPD.ownership.valueOf(rs.getString("Ownership")
                        )));
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }

        return arProduct;

    }//Returning all selected information by their Province Code

    public static double CalculateTotalSales() throws NotFoundException {
        double sum = 0.0;
        try {
            ps = con.prepareStatement("select Sum(Sales) AS Sum FROM tblAdelaneFashions");
            rs = ps.executeQuery();
            while (rs.next()) {
                sum = rs.getDouble("Sum");
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());

        }
        return sum;
    }//Calculating Total Sales

    public static double CalculateTotalCompanyOwnedSales() throws NotFoundException {
        double sum = 0.0;
        try {
            ps = con.prepareStatement("select Sum(Sales) AS Sum FROM tblAdelaneFashions"
                    + " WHERE Ownership= 'C'");
            rs = ps.executeQuery();
            while (rs.next()) {
                sum = rs.getDouble("Sum");
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());

        }
        return sum;
    }//Calculating Total sales for Company Owned 

    public static double CalculateTotalFranchiseSales() throws NotFoundException {
        double sum = 0.0;
        try {
            ps = con.prepareStatement("select Sum(Sales) AS Sum FROM tblAdelaneFashions "
                    + "WHERE Ownership= 'F'");
            rs = ps.executeQuery();
            while (rs.next()) {
                sum = rs.getDouble("Sum");
            }
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());

        }
        return sum;
    }//Calculating Total sales for Company Franchise

    public static void RemoveStore(String StoreNo) throws NotFoundException {

        try {
            ps = con.prepareStatement("delete From tblAdelaneFashions WHERE StoreNumber = ?");
            ps.setString(1, StoreNo);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }

    }//Removing Data from where store number is selected

    public static void addNewStore(AdelaneFashionsPD objPD) throws NotFoundException {
        try {
            ps = con.prepareStatement("insert into tblAdelaneFashions(StoreNumber,City,Sales,Province,Ownership)"
                    + " values(?,?,?,?,?)");
            ps.setString(1, objPD.getStoreNo());
            ps.setString(2, objPD.getCity());
            ps.setDouble(3, objPD.getSales());
            ps.setString(4, String.valueOf(objPD.getProvince()));
            ps.setString(5, String.valueOf(objPD.getOwnership())); 
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
    }//Adding information to the database

    public static void IncreasePrices(double percentage) throws NotFoundException {
        double perc = percentage / 100;
        try {
            ps = con.prepareStatement("UPDATE tblAdelaneFashions "
                    + "SET Sales = Sales + (Sales * ? )"
                    + " WHERE Province = 'GP' AND Ownership = 'F'");
            ps.setDouble(1, perc);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new NotFoundException("No Data Found \n " + e.getMessage());
        }

    }//Updating Sales Where Province is GP and Ownership is Franchised
    
    

    public static ArrayList<String> ReturnStoreNo() throws NotFoundException {
        arStoreNo.clear();
        try {
            ps = con.prepareStatement("Select StoreNumber from tblAdelaneFashions");
            rs = ps.executeQuery();
            while (rs.next()) {
                arStoreNo.add(rs.getString(1));
            }
        } catch (SQLException e) {
           throw new NotFoundException("No Data Found \n " + e.getMessage());
        }
        return arStoreNo;
    }//Selecting Store number from database
}
